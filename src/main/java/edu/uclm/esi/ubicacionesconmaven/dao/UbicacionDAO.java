package edu.uclm.esi.ubicacionesconmaven.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import edu.uclm.esi.ubicacionesconmaven.model.Ubicacion;

public interface UbicacionDAO extends CrudRepository<Ubicacion, Integer>{

	@Query("Select id, nombre from Ubicacion where tipo='Comunidad autónoma' order by nombre")
	List<Ubicacion> findCCAA();

	@Query("Select id, nombre from Ubicacion where tipo='Provincia' and idPadre=?1 order by nombre")
	List<Ubicacion> findProvincias(Integer idPadre);

	@Query("Select id, nombre from Ubicacion where tipo='Municipio' and idPadre=?1 order by nombre")
	List<Ubicacion> findMunicipios(Integer idPadre);
}
