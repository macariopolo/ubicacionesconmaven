package edu.uclm.esi.ubicacionesconmaven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LanzadoraUbicaciones {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(LanzadoraUbicaciones.class, args);
	}

}
