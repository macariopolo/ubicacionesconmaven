package edu.uclm.esi.ubicacionesconmaven.web.controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import edu.uclm.esi.ubicacionesconmaven.dao.UbicacionDAO;
import edu.uclm.esi.ubicacionesconmaven.model.Ubicacion;

@RestController
public class WebController {
	@Autowired
	private UbicacionDAO dao;
	
	@GetMapping("/deDondeSoy")
	@CrossOrigin(origins = "*")
	public String deDondeSoy(HttpServletRequest request, HttpServletResponse response) {
		response.addHeader("Access-Control-Allow-Credentials", "true");
		Cookie[] cookies = request.getCookies();
		if (cookies!=null) {
			Cookie comunidad = findCookie("comunidad", cookies);
			Cookie provincia = findCookie("provincia", cookies);
			Cookie municipio = findCookie("municipio", cookies);
			if (comunidad!=null) {
				return comunidad.getValue() + ", " + provincia.getValue() + ", " + municipio.getValue();
			}
		}
		return "No lo sé, no tienes cookies";
	}
	
	@GetMapping(value="/getCCAA", produces="application/json")
	@CrossOrigin(origins = "*")
	public List<Ubicacion> getCCAA(HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<Ubicacion> result = dao.findCCAA();
		response.addHeader("Access-Control-Allow-Credentials", "true");
		response.setHeader("Access-Control-Allow-Origin", request.getHeader("origin"));
		return result;
	}

	@GetMapping(value="/getProvincias", produces="application/json")
	@CrossOrigin(origins = "*")
	public List<Ubicacion> getProvincias(HttpServletRequest request, HttpServletResponse response, @RequestParam Integer idPadre) throws Exception {
		List<Ubicacion> result = dao.findProvincias(idPadre);
		response.setHeader("Access-Control-Allow-Credentials", "true");
		response.setHeader("Access-Control-Allow-Origin", request.getHeader("origin"));
		return result;
	}
	
	@GetMapping(value="/getMunicipios", produces="application/json")
	@CrossOrigin(origins = "*")
	public List<Ubicacion> getMunicipios(HttpServletRequest request, HttpServletResponse response, @RequestParam Integer idPadre) throws Exception {
		List<Ubicacion> result = dao.findMunicipios(idPadre);
		response.setHeader("Access-Control-Allow-Credentials", "true");
		response.setHeader("Access-Control-Allow-Origin", request.getHeader("origin"));
		return result;
	}
	
	@PostMapping(value="/registrar")
	@CrossOrigin(origins="http://localhost:9100", allowCredentials="true")
	public void registrar(HttpServletRequest request, HttpServletResponse response, @RequestBody Map<String, Object> datosUsuario) throws Exception {
		response.setHeader("Access-Control-Allow-Credentials", "true");
		System.out.println(request.getHeader("origin"));
		JSONObject jso = new JSONObject(datosUsuario);
		Cookie ccaa = new Cookie("comunidad", jso.getJSONArray("comunidad").getString(1));
		ccaa.setMaxAge(60*60*24*365);
		Cookie provincia = new Cookie("provincia", jso.getJSONArray("provincia").getString(1));
		provincia.setMaxAge(60*60*24*365);
		Cookie municipio =new Cookie("municipio", jso.getJSONArray("municipio").getString(1));
		municipio.setMaxAge(60*60*24*365);
		response.addCookie(ccaa);
		response.addCookie(provincia);
		response.addCookie(municipio);
	}
	
	@ExceptionHandler(Exception.class)
	public ModelAndView handleException(HttpServletRequest req, Exception ex) {
		ModelAndView result = new ModelAndView();
		result.setViewName("respuesta");
		result.addObject("exception", ex);
		result.setStatus(HttpStatus.BAD_REQUEST);
	    return result;
	}
	

	
	private Cookie findCookie(String name, Cookie[] cookies) {
		for (Cookie cookie : cookies)
			if (cookie.getName().equals(name))
				return cookie;
		return null;
	}
}
