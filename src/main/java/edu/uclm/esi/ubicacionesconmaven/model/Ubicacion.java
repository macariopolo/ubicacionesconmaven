package edu.uclm.esi.ubicacionesconmaven.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Ubicacion {
	@Id
	private Integer id;
	private String nombre;
	private String tipo;
	private int idPadre;
	
	public Ubicacion() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getIdPadre() {
		return idPadre;
	}

	public void setIdPadre(int idPadre) {
		this.idPadre = idPadre;
	}
}
