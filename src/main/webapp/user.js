function UserViewModel() {
	self = this;

	this.userName = ko.observable();
	this.pwd1 = ko.observable();
	this.pwd2 = ko.observable();
	this.pwd = ko.observable();
	
	this.comunidadesAutonomas=ko.observableArray([]);
	this.comunidadAutonoma = ko.observable();
	this.provincias=ko.observableArray([]);
	this.provincia = ko.observable();
	this.municipios=ko.observableArray([]);
	this.municipio = ko.observable();
	this.miUbicacion = ko.observable();
	
	function getCCAA() {
		var data = {
			url : "getCCAA",
			type : "get",
			success : function(resp) {
				self.comunidadesAutonomas(resp)
			},
			error : function(resp) {
				alert("Error");
			}
		};
		$.ajax(data);
	}
	
	getCCAA();
	
	self.getProvincias = function() {
		var id = self.comunidadAutonoma()[0];
		var data = {
			url : "getProvincias?idPadre=" + id,
			type : "get",
			success : function(resp) {
				self.provincias(resp)
			},
			error : function(resp) {
				alert("Error");
			}
		};
		$.ajax(data);
	}
	
	self.getMunicipios = function() {
		var id = self.provincia()[0];
		var data = {
			url : "getMunicipios?idPadre=" + id,
			type : "get",
			success : function(resp) {
				self.municipios(resp)
			},
			error : function(resp) {
				alert("Error");
			}
		};
		$.ajax(data);
	}
	
	self.registrar = function() {
		var info = {
			comunidad : self.comunidadAutonoma(),
			provincia : self.provincia(),
			municipio : self.municipio()
		};
		var data = {
			url : "registrar",
			contentType : "application/json",
			data : JSON.stringify(info),
			type : "post",
			success : function(resp) {
				alert("Registrado.");
			},
			error : function(resp) {
				alert("Error");
			}
		};
		$.ajax(data);
	}
	
	self.deDondeSoy = function() {
		var data = {
			url : "deDondeSoy",
			type : "get",
			success : function(resp) {
				self.miUbicacion(resp)
			},
			error : function(resp) {
				alert("Error");
			}
		};
		$.ajax(data);
	}
}

var user = new UserViewModel();
ko.applyBindings(user);